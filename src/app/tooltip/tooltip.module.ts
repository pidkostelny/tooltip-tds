import { NgModule } from '@angular/core';
import { TooltipDirective } from './tooltip.directive';
import {OverlayModule} from '@angular/cdk/overlay';
import { TooltipComponent } from './tooltip.component';



@NgModule({
  declarations: [TooltipDirective, TooltipComponent],
  exports: [
    TooltipDirective,
  ],
  imports: [
    OverlayModule,
  ],
})
export class TooltipModule { }
