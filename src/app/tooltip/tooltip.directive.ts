import {ComponentRef, Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
import {ComponentPortal} from '@angular/cdk/portal';
import {Overlay, OverlayPositionBuilder, OverlayRef} from '@angular/cdk/overlay';
import {TooltipComponent} from './tooltip.component';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements OnInit {

  @Input('appTooltip') text = '';
  private overlayRef: OverlayRef;

  constructor(
    private overlay: Overlay,
    private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef
  ) {}

  ngOnInit() {
    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([{
        originX: 'center',
        originY: 'bottom',
        overlayX: 'center',
        overlayY: 'top',
        offsetY: 8,
      }]);

    this.overlayRef = this.overlay.create({ positionStrategy });
  }

  @HostListener('mouseenter')
  onHover() {
    const portal = new ComponentPortal(TooltipComponent);
    const tooltipRef: ComponentRef<TooltipComponent> = this.overlayRef.attach(portal);
    tooltipRef.instance.text = this.text;
  }

  @HostListener('mouseout')
  onMouseOut() {
    this.overlayRef.detach();
  }

  @HostListener('document:click', ['$event.target'])
  onClickOut(target) {
    if (!this.elementRef.nativeElement.contains(target)) {
      this.overlayRef.detach();
    }
  }
}
